********``# Qualité des Données

**le contexte**
Il s'agit du relevée de température de chaque mois de l'année d'une ville. 
Pour chaque jour, il s'agit des température moyenne constatés entre 8h et 18h.

**l'analyse du sujet**
Certaines données sont incomplétes, ce qui entraîne des incohérences de données.
Parfois on peut remarquer une grosse variation de température entre un jour et le lendemain, ou des températures négative en été. 
Ce n'est pas possible.
Il faut donc modifier ces données afin de leur données de la cohérences, et ainsi pouvoir les exploiter.

**votre  démarche de résolution**
On se rend compte que des données sont incohérentes quand elle on un format d'erreur 0xFFF ou SUN par exemple. 
Quand les données sont du simple au double, au données de l'aveil alors on modifie ces données par un moyenne des données obtenu les jours précédent.

**vos conclusions**
Les données doivent être le plus complétes possible afin de pouvoir les interprétés.

capture d'écran significatif****
[general][]
(url https://gitlab.com/vadrouet14/qualite-des-donnees/-/blob/master/general.png)

[maximum]
(url)https://gitlab.com/vadrouet14/qualite-des-donnees/-/blob/master/maximum.png

[minimum]
(url)https://gitlab.com/vadrouet14/qualite-des-donnees/-/blob/master/minimum.png

[écart_type]
(url)https://gitlab.com/vadrouet14/qualite-des-donnees/-/blob/master/écart_type.png

[répartition par mois]
(url)https://gitlab.com/vadrouet14/qualite-des-donnees/-/blob/master/janvier.png

La répartition par moi permet d'avoir une meilleur idée de la représentation, que si on utilise uniquement la représentation globale. 
On peut remarquer que les température sont souvent changantes, d'un jour à l'autre. Durant chaque moi il y a plus de 10 degré, d'écart entre les différentes températures.

[répartition sur l'année]
(url)https://gitlab.com/vadrouet14/qualite-des-donnees/-/blob/master/année.png

[données modifier]
(url)https://gitlab.com/vadrouet14/qualite-des-donnees/-/blob/master/data.png
On remarque que certaines données ont des format incorectes (0xFFF ou encore SUN), parfois il y a des température impossible comme 48 degré l'été ou encore -33 lhiver. 
Cela met en lumière l'importance d'avoir des données exact, fiable, non modifiable.




